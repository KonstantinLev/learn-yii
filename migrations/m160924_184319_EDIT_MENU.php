<?php

use yii\db\Migration;

class m160924_184319_EDIT_MENU extends Migration
{
    public function up()
    {
        $this->update('menu', ['link' => 'main/index'], 'id = 1');
        $this->update('menu', ['link' => 'main/products'], 'id = 2');
        $this->update('menu', ['link' => 'main/about'], 'id = 3');
        $this->update('menu', ['link' => 'main/contacts'], 'id = 4');
        $this->update('menu', ['link' => 'main/order'], 'id = 5');
    }

    public function down()
    {
        $this->update('menu', ['link' => 'main'], 'id = 1');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
