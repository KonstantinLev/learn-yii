<?php

use yii\db\Migration;

class m161006_235535_CREATE_TABLE_SEF extends Migration
{
    public function up()
    {
        $this->createTable('sef', [
            'id' => $this->primaryKey(),
            'link' => $this->string(255)->comment('Контроллер и экшен'),
            'link_sef' => $this->string(255)->comment('ЧПУ ссылка')
        ]);

        $this->batchInsert('sef', ['link', 'link_sef'], [
           ['main/index', 'mozno-pisat-chto-ugodno'],
           ['products/?cat=1', 'soft-furniture'],
           ['products/?cat=1&sect=1', 'sofa'],
           ['products/?cat=1&sect=2', 'chair'],
           ['products/?cat=1&sect=3', 'ottoman'],
           ['products/?cat=2', 'living-room'],
           ['products/?cat=2&sect=4', 'coffee-table'],
           ['products/?cat=2&sect=5', 'wall'],
           ['products/?cat=2&sect=6', 'soft-furniture'],
           ['products/?cat=1', 'household'],
        ]);
    }

    public function down()
    {
        $this->dropTable('sef');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
