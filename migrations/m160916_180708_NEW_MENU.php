<?php

use yii\db\Migration;

class m160916_180708_NEW_MENU extends Migration
{
    public function up()
    {
        $this->dropTable('menu');
        $this->createTable('menu', [
           'id' => $this->primaryKey(),
            'title' => $this->string(255)->comment('Название элемента меню'),
            'parent_id' => $this->integer(),
            'link' => $this->string(255)
        ]);

        $this->batchInsert('menu', ['title', 'parent_id', 'link'], [
            ['Главная', null, 'index'],
            ['Продукция', null, 'products'],
            ['О компании', null, 'about'],
            ['Контакты', null, 'contacts'],
            ['Ваш заказ', null, 'order'],
            ['Мягкая мебель', 2, 'products/?cat=1'],
            ['Гостиная', 2, 'products/?cat=2'],
            ['Спальня', 2, 'products/?cat=3'],
            ['Столовая', 2, 'products/?cat=4'],
            ['Прихожая', 2, 'products/?cat=5'],
            ['Офис', 2, 'products/?cat=6'],
            ['Диваны', 6, 'products/?cat=1&sect=1'],
            ['Кресла', 6, 'products/?cat=1&sect=2'],
            ['Пуфики', 6, 'products/?cat=1&sect=3'],
            ['Журнальные столики', 7, 'products/?cat=2&sect=4'],
            ['Стенки', 7, 'products/?cat=2&sect=5'],
            ['Тумбы под телевизор', 7, 'products/?cat=2&sect=6'],
            ['Гарнитуры для спальни', 8, 'products/?cat=3&sect=7'],
            ['Кровати', 8, 'products/?cat=3&sect=8'],
            ['Тумбы и комоды для спальни', 8, 'products/?cat=3&sect=9'],
            ['Зеркала', 8, 'products/?cat=3&sect=10'],
            ['Обеденные столы', 9, 'products/?cat=4&sect=11'],
            ['Стулья', 9, 'products/?cat=4&sect=12'],
            ['Табуреты', 9, 'products/?cat=4&sect=13'],
            ['Кухонные гарнитуры', 9, 'products/?cat=4&sect=14'],
            ['Наборы мебели для прихожей', 10, 'products/?cat=5&sect=15'],
            ['Тумбы для обуви и комоды', 10, 'products/?cat=5&sect=16'],
            ['Шкафы и стеллажи', 10, 'products/?cat=5&sect=17'],
            ['Офисные кресла', 11, 'products/?cat=6&sect=18'],
            ['Письменные и компьютерные столы', 11, 'products/?cat=6&sect=19'],
            ['Оперативная офисная мебель', 11, 'products/?cat=6&sect=20'],
        ]);
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
