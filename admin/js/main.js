var modalEditProduct = $('#modal-edit-product');

$('document').ready(function(){
    modalEditProduct.modal({show: false});
});


function editProducts(e)
{
    var url;
    url = 'edit-products/update?id='+$(e).data('id-product');

    $.ajax({
        url: url,
        type: 'GET',
        async: false,
        showLoader: true,
        success: function (data) {
            console.log(data);
            try{
                $('.update-product').html(data);
                modalEditProduct.modal('show');
            }
            catch(e)
            {
                console.log(e);
                return;
            }
        }
    });
}



