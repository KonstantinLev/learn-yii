<?php

namespace app\models;

use app\models\Products;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * ProductsSearch represents the model behind the search form about `app\models\Products`.
 */
class ProductsSearch extends Products
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'fid_category', 'fid_section', 'fid_age'], 'integer'],
            [['title', 'descr', 'articul', 'link', 'date_add', 'img_path_small', 'img_path_full'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        //если нам нужно приджойнить некоторые таблицы - юзаем joinWith и передаем ему в параметры связь,
        //реализованную в данной модели. Например у Products есть метод getFidCategory, в котором прописан
        //hasOne на таблицу categorys. В joinWith мы происываем этот метод, только без префикса get и начиная с
        //маленькой буквы. Это обязательно. Также допустима такая запись joinWith('fidCategory.fidSection'),
        //т.е. если у модели, которые мы приджойниваем тоже есть какие-либо связи, то можно обращаться по цепочке
        //символ '.'
        $query = self::find()
                    ->joinWith('fidCategory')
                    ->joinWith('fidSection');

        // add conditions that should always apply here

        //про более подробные настройки читать в доках
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 15
            ]
        ]);

        //Загружаем в данную модель параметры(приходят гет или пост)
        $this->load($params);

        //если какая либо валидация не прошла, то возвращаем что есть (на экране будут появляться сообщеня, что
        // не заполнено то или др и тп)
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        //здесь формируются фильтры типа интегер
        $query->andFilterWhere([
            'id' => $this->id,
            'fid_category' => $this->fid_category,
            'fid_section' => $this->fid_section,
            'price' => $this->price,
            'date_add' => $this->date_add,
            'fid_age' => $this->fid_age,
        ]);

        //здесь фильтры на текстовые поля
        $query
            //->andFilterWhere(['like', new Expression('lower(trim(title))'), mb_strtolower(trim($this->title))])
            ->andFilterWhere(['like', self::tableName().'.title', mb_strtolower(trim($this->title))])
            ->andFilterWhere(['like', 'descr', $this->descr])
            ->andFilterWhere(['like', 'articul', $this->articul])
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'img_path_small', $this->img_path_small])
            ->andFilterWhere(['like', 'img_path_full', $this->img_path_full]);

        return $dataProvider;
    }
}
