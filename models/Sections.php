<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "sections".
 *
 * @property string $id
 * @property string $title
 *
 * @property Products[] $products
 */
class Sections extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sections';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['fid_section' => 'id']);
    }

    public static function getNameSectionById($id)
    {
        return self::find()->where(['id' => $id])->one();
    }

    public static function getSections()
    {
        $data = self::find()->all();
        return ArrayHelper::map($data, 'id', 'title');
    }
}
