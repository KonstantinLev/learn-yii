<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "categorys".
 *
 * @property string $id
 * @property string $title
 *
 * @property Products[] $products
 */
class Categorys extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categorys';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['fid_category' => 'id']);
    }

    public static function getNameCategoryById($id)
    {
        return self::find()->where(['id' => $id])->one();
    }

    public static function getCategorys()
    {
        $data = self::find()->all();
        return ArrayHelper::map($data, 'id', 'title');
    }
}
