<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Orders;

/**
 * OrdersSearch represents the model behind the search form about `app\models\Orders`.
 */
class OrdersSearch extends Orders
{

    public $date_from;
    public $date_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['fid_products', 'email', 'phone', 'name', 'notice', 'date_order', 'date_send', 'date_pay', 'date_from', 'date_to'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $date_array = explode(' - ', $this->date_order);
        $this->date_from = $date_array[0];
        $this->date_to = $date_array[1];

        var_dump($this->date_from);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            //'date_order' => $this->date_order,
            'date_send' => $this->date_send,
            'date_pay' => $this->date_pay,
        ]);
        $query->andFilterWhere([
            'between', 'date_order', $this->date_from, $this->date_to

        ]);

        $query->andFilterWhere(['like', 'fid_products', $this->fid_products])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'notice', $this->notice]);

        //die($query->createCommand(Orders::getDb())->getRawSql());
        return $dataProvider;
    }
}
