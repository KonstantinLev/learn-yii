<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property string $title
 * @property integer $parent_id
 * @property string $link
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id'], 'integer'],
            [['title', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'parent_id' => 'Parent ID',
            'link' => 'Link',
        ];
    }

    public function getChildren()
    {
        return $this->hasMany(Menu::className(), ['parent_id' => 'id']);
    }

    public static function getMenu()
    {
        return self::find()->where(['parent_id' => null])->all();
    }

    public static function getRightMenu()
    {
        return self::find()->where(['id' => 2])->one()->children;
    }

    public static function buildMenu($child = false)
    {
        $items = [];
        $count = 0;
        $menu = $child ? $child : self::getMenu();
        foreach($menu as $item_menu) {
            $items[] = ['label' => $item_menu->title];
            if($child) $items[$count]['url'] = [$item_menu->link];
            if(!empty($item_menu->children)){
                $items[$count]['submenuOptions'] = ['class' => 'dropdown dropdown-menu'];
                $items[$count]['items'] = self::buildMenu($item_menu->children);
            } else {
                $items[$count]['url'] = [$item_menu->link];
            }
            $count++;
        }
        return $items;
    }

    public static function buildRightMenu($child = false)
    {
        $items = [];
        $count = 0;
        $menu = $child ? $child : self::getRightMenu();
        foreach($menu as $item_menu) {
            if($child) $items[$count]['url'] = ['route' => $item_menu->link];
            else $items[] = ['url' => []];
            $items[$count]['label'] = $item_menu->title;
            if(!empty($item_menu->children)){
                $children = self::buildRightMenu($item_menu->children);
                for($i = 0; $i < count($children); $i ++){
                    $items[$count][] = $children[$i];
                }
            }
            $count++;
        }
        return $items;
    }

}
