<?php

namespace app\models;

use yii\base\Model;

class Search extends Model
{
    public $search;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['search'], 'string'],
        ];
    }

}
