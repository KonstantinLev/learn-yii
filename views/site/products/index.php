<?php
use yii\bootstrap\Button;
use yii\bootstrap\Html;
use yii\widgets\LinkPager;
use yii\widgets\Breadcrumbs;

/* @var $catName app\models\Categorys*/
/* @var $sectName app\models\Sections*/
/* @var $models app\models\Products*/
/* @var $pages yii\data\Pagination*/

if(!empty($sectName)){
    $this->params['breadcrumbs'][] = ['label' => $catName->title, 'url'=> ['products/?cat='.$catName->id]];
    $this->params['breadcrumbs'][] = $sectName->title;
} else {
    $this->params['breadcrumbs'][] = $catName->title;
}


?>
<?=  Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [], 'homeLink' => ['label' => 'Главная', 'url' => '/'] ])?>
<h2 class="titleText">Мягкая мебель</h2>
<div class="goods">
    <div class="row">
        <?php foreach ($models as $product) { ?>
            <div class="col-md-4">
                <div class="good-block">
                    <?= Html::a('', ['products/view-detail', 'id' => $product->id], ['class' => 'good-block-image', 'style' => 'background-image: url("'.\yii\helpers\Url::to($product->pathImgSmall).'")'])?>
                    <div class="wrap">
                        <span class="price"><?=$product->price?><span>р</span></span>
                        <div class="order">
                            <?= Html::a('Добавить в заказ', '', ['class' => 'small-button', 'data-id' => $product->id, 'onclick' => 'javascript: addToOrder(this);'])?>
                            <!--                            --><?//= Button::Widget([
                            //                                'label'=>'Добавить в заказ',
                            //                                'options'=>['class' => 'small-button disabled btn', 'data-id' => $product->id, 'onclick' => 'javascript: addToOrder(this);']
                            //                            ]); ?>
                        </div>
                    </div>
                    <div class="title-good">
                        <h4>
                            <?= Html::a($product->title, ['products/view-detail', 'id' => $product->id], ['class' => 'myLink'])?>
                        </h4>

                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<?= LinkPager::widget([
    'pagination' => $pages,
]); ?>

