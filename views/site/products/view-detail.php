<?php

/* @var $product app\models\Products*/
use yii\bootstrap\Button;
use yii\bootstrap\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;

$this->params['breadcrumbs'][] = ['label' => $product->fidCategory->title, 'url'=> ['products/?cat='.$product->fid_category]];
$this->params['breadcrumbs'][] = ['label' => $product->fidSection->title, 'url'=> ['products/?cat='.$product->fid_category.'&sect='.$product->fid_section]];
$this->params['breadcrumbs'][] = 'артикул '.$product->articul;
?>
<h2 class="titleText">Описание товара</h2>
<div class="descr-good">
    <div class="row">
        <div class="col-md-8">
            <?=  Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [], 'homeLink' => ['label' => 'Главная', 'url' => '/'] ])?>
            <div class="good-block">
                <h3><?=$product->title?></h3>
                <?= Html::a('', ['products/photo', 'id' => $product->id], ['class' => 'good-block-image', 'style' => 'background-image: url("'.\yii\helpers\Url::to($product->pathImgSmall).'")'])?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="wrap">
                <div class="order">

                        <?= Html::a('Добавить в заказ', '', ['class' => 'small-button', 'data-id' => $product->id, 'onclick' => 'javascript: addToOrder(this);'])?>
<!--                    --><?//= Button::Widget([
//                    'label'=>'Добавить в заказ',
//                    'options'=>['class' => 'small-button', 'data-id' => $product->id, 'onclick' => 'javascript: addToOrder(this);']
//                    ]); ?>


                </div>
                <div class="sale">
                    <div class="price"><?=$product->price?><span>р</span></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="character-good">
    <div class="row">
        <div class="col-md-6">
            <h3>Описание</h3>
            <div class="character-good-descr">
                <p>
                    <?=$product->descr?>
                </p>
            </div>
        </div>
        <div class="col-md-6">
            <h3>Характеристики</h3>
            <div class="props-list clearfix">
                <table class="table table-striped">
                    <tbody>
                    <?php foreach($product->productSpecificLists as $specific) { ?>
                        <tr>
                            <td class="name-prop"><?=$specific->fidSpecific->title?></td>
                            <td class="value-prop"><?=$specific->value?></td>
                        </tr>
                    <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
