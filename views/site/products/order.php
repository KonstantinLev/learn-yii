<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
/* @var $products app\models\Products*/
/* @var $orderModel app\models\Orders*/

$session = Yii::$app->session;
$session->open();
?>

<h2 class="titleText">Ваш заказ</h2>
<?php if(count($_SESSION['order']) > 0) { ?>

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
    ]); ?>

    <?php foreach($products as $product) { ?>
        <div class="order_block">
            <div class="img_prod" style="background-image: url(<?= \yii\helpers\Url::to($product->pathImgSmall) ?>)"></div>
            <h3><?=$product->title?></h3>
            <div class="count">
                <?= $form->field($orderModel, 'price')->hiddenInput(['name' => "price[$product->id]" ,'value' => $product->price])->label(false) ?>
                <?= $form->field($orderModel, 'count_prod')->hiddenInput(['name' => "count[$product->id]", 'class' => 'c_prod'])->label(false) ?>
                Кол-во
                <span class="glyphicon glyphicon-menu-left" name="left" onclick="counter(this);"></span>
                <span class="count_prod">1</span>
                <span class="glyphicon glyphicon-menu-right" name="right" onclick="counter(this);"></span>
                | <span class="price"><?=$product->price?></span><span>р</span>
            </div>
            <div class="remove-item">
                    <?= Html::a('удалить', ['products/order', 'remove' => $product->id], [])?>
            </div>
        </div>
        <hr>
    <?php } ?>

    <div class="itogo">
        <h3>Сумма заказа: </h3>
        <span></span>р
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <h4 style="text-align: center;">Для оформления заказа укажите Ваше имя, телефон и примечания, если такие имеются.<br> Наши операторы свяжутся с Вами для уточнения заказа в ближайшее время!</h4>
        </div>
        <div class="col-md-6 col-md-offset-3">
            <p style="color: red" id="order_error"></p>

            <?= $form->field($orderModel, 'name')->textInput() ?>
            <?= $form->field($orderModel, 'phone')->textInput() ?>
            <?= $form->field($orderModel, 'email')->textInput() ?>
            <?= $form->field($orderModel, 'notice')->textInput() ?>

            <?= Html::submitButton('Оформить заказ', ['class' => 'small-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>



<?php } elseif($processed === true) { ?>
    <h3>Заказ оформлен! Ожидайте звонка Менеджера</h3>
<?php } else { ?>
    <h3>Пока Вы ничего не добавили в свой заказ</h3>
<?php } ?>
