<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/* @var $model app\models\Search*/
/* @var $products app\models\Products*/

$this->registerJsFile('@web/js/search.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/main.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<h2 class="titleText">Поиск товаров</h2>
<div class="block-search">
    <?php $form = ActiveForm::begin([
        'id' => 'current-form',
        'method' => 'get',
        'options' => ['class' => 'form-horizontal'],
    ]); ?>

       <?= $form->field($model, 'search')->widget(\yii\jui\AutoComplete::classname(), [
            'options' => [
                'placeholder' => 'Начните вводить...',
                'class' => 'form-control product-name',
                'onkeyup' => "javascript:if (event.keyCode != '13') searchProducts(this);"
            ],
            'clientOptions' => [
                'source'=> [],
                'autoFill'=>true,
                'minLength'=>'3',
            ]
        ])->label(false) ?>

        <?= Html::button('Поиск', ['class' => 'btn btn-success btn-search', 'onclick' => '$.pjax.reload({
                                container:"#search-list",
                                replace: false,
                                push: false,
                                url: "index?" + $("#current-form").serialize()
                             });']) ?>

    <?php ActiveForm::end(); ?>
</div>
<?php $this->registerJs('$.pjax.defaults.timeout = 15000;$.pjax.defaults.push = false;', \yii\web\View::POS_READY); ?>
<?php Pjax::begin(['id' => 'search-list']); ?>
    <?php if(!$products) { ?>
         <h2>Ничего не найдено...</h2>
    <?php } else { ?>
        <div class="goods">
            <div class="row">
                <?php foreach ($products as $product) { ?>
                    <div class="col-md-4">
                        <div class="good-block">
                            <?= Html::a('', ['products/view-detail', 'id' => $product->id], ['class' => 'good-block-image', 'style' => 'background-image: url("'.\yii\helpers\Url::to($product->pathImgSmall).'")'])?>
                            <div class="wrap">
                                <span class="price"><?=$product->price?><span>р</span></span>
                                <div class="order">
                                    <?= Html::a('Добавить в заказ', '', ['class' => 'small-button', 'data-id' => $product->id, 'onclick' => 'javascript: addToOrder(this);'])?>

                                </div>
                            </div>
                            <div class="title-good">
                                <h4>
                                    <?= Html::a($product->title, ['products/view-detail', 'id' => $product->id], ['class' => 'myLink'])?>
                                </h4>

                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

<?php Pjax::end(); ?>