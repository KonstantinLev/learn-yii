<?php
/* @var $mainProducts app\models\Products*/
use yii\bootstrap\Html;
use yii\bootstrap\Modal;

//var_dump($mainProducts);
?>
<h2 class="titleText">Новые поступления</h2>
<div class="stock-day">
    <div class="row">
        <?php $i = 0; foreach($mainProducts as $product) { ?>
            <?php if($i == 0) { ?>
                <div class="col-md-8">
                    <div class="big-block">
                        <?= Html::a('', ['/products/view-detail?id='.$product->id], ['class' => 'big-block-image', 'style' => 'background-image: url("'.\yii\helpers\Url::to($product->pathImgSmall).'")'])?>
                        <div class="wrap">
                            <h4><?=$product->title?></h4>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="col-md-4">
                    <div class="small-block">
                        <?= Html::a('', ['/products/view-detail?id='.$product->id], ['class' => 'small-block-image', 'style' => 'background-image: url("'.\yii\helpers\Url::to($product->pathImgSmall).'")'])?>
                        <div class="wrap">
                            <h4><?=$product->title?></h4>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php $i++; } ?>
    </div>
</div>
<?php Modal::begin([
'header' => '<h2>Hello world</h2>',
'toggleButton' => ['label' => 'click me'],
]); ?>
<h3>fgdlggfdg</h3>
<?php echo 'Say hello...'; ?>

<?php Modal::end(); ?>

<div class="podbor-furniture">
    <h2 class="titleText">Индивидуальный подбор мебели</h2>
    <p>Наша специальная система поможет Вам выбрать мебель, учитывая Ваши желания и предпочтения.</p>
    <p>Вам нужно всего лишь заполнить анкету и мы предоставим Вам возможные варианты мебели, которые могут Вас заинтересовать!</p>
    <p><?= Html::a('Подобрать мебель',['main/index'], ['class' => 'small-button']) ?></p>
</div>
