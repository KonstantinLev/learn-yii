<?php

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $writeUsModel app\models\WriteUsList */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use host33\multilevelverticalmenu\MultilevelVerticalMenu;
use yii\bootstrap\ActiveForm;
use app\components\ProductsOthers;


$writeUsModel = $this->params['writeUsModel'];
$session = Yii::$app->session;
$session->open();

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->urlManager->createUrl(['main/index']),
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => \app\models\Menu::buildMenu(),
    ]);

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => [

            ['label' => 'Ваш заказ('.count($_SESSION['order']).')', 'url' => ['/products/order'], 'options' => ['class' => 'badge']],
        ],
    ]);

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Поиск', 'url' => ['/search/index']],
            Yii::$app->user->isGuest ? (
            ['label' => 'Войти в личный кабинет', 'url' => ['/main/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/main/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>
    <div class="content-slider">
        <div class="row">
            <div class="col-md-12">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Маркеры слайдов -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>

                    <!-- Содержимое слайдов -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <?=Html::img('@web/files/img/slider/1.jpg', ['alt'=> 'Слайд'])?>
                            <div class="carousel-caption">
                                <h3>Мебель</h3>
                                <p>Для <span class="accent-color">вашего</span> дома и уюта</p>
                            </div>
                        </div>

                        <div class="item">
                            <?=Html::img('@web/files/img/slider/2.jpg', ['alt'=> 'Слайд'])?>
                            <div class="carousel-caption">
                                <h3>Мебель</h3>
                                <p>Для <span class="accent-color">вашего</span> дома и уюта</p>
                            </div>
                        </div>

                        <div class="item">
                            <?=Html::img('@web/files/img/slider/3.jpg', ['alt'=> 'Слайд'])?>
                            <div class="carousel-caption">
                                <h3>Мебель</h3>
                                <p>Для <span class="accent-color">вашего</span> дома и уюта</p>
                            </div>
                        </div>
                    </div>

                    <!-- управление слайдером -->
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="">Виджет - <?=\app\components\Hello::widget(['message' => 'dasdsa'])?></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="message_site" style="text-align: center;"><h2></h2></div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h2 class="titleText">Каталог товаров</h2>

                <?php  echo MultilevelVerticalMenu::widget(
                  [
                      "menu"=> \app\models\Menu::buildRightMenu(),
                      "transition" => 1 // To choose between 1,2,3,4 and 5.
                  ]
                ); ?>

                <div class="write-us">
                    <h2 class="titleText">Напишите нам</h2>
                    <p style="color: red" id="write-us-error"></p>

                    <?php $form = ActiveForm::begin([
                        'id' => 'write-us-form',
                        'options' => ['class' => 'form-horizontal'],
                    ]); ?>

                    <?= $form->field($writeUsModel, 'name')->textInput() ?>
                    <?= $form->field($writeUsModel, 'email')->textInput() ?>
                    <?= $form->field($writeUsModel, 'phone')->textInput() ?>
                    <?= $form->field($writeUsModel, 'text')->textInput() ?>

                    <?= Html::submitButton('Отправить', ['class' => 'small-button']) ?>

                    <?php ActiveForm::end(); ?>

                </div>
                <?php if(Yii::$app->controller->action == 'view-detail') { $id = Yii::$app->request->get('id');} ?>
                <h2 class="titleText">Другие товары</h2>
                <?=ProductsOthers::widget(['id' => $id, 'count' => 2, 'border' => 'red'])?>

            </div>
            <div class="col-md-9">
                <div class="main-content">
                    <?= $content ?>
                </div>
            </div>
        </div>

    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <h3><a href="#" class="navbar-brand">ооо Полюс</a></h3>
            </div>
            <div class="col-md-4">
                <p>Наш телефон: +7 (589) 958-58-58</p>
                <p>Все права защищены, 2016г.</p>
            </div>
            <div class="col-md-6">
                <p>&copy; Компания ООО ПОЛЮС, мебель для вашего дома и уюта.</p>
                <p>Мы находмся по адресу:</p>
                <span>г. Рославль, ул Ленина 56, офис 512</span>
            </div>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
