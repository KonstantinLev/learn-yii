<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Редактор товаров';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{update} | {delete}',
        'buttons' => [
            'update' => function($key, $model, $url){
                return Html::a('Редактировать', '#',
                    [
                        'class' => 'btn btn-default',
                        'onclick' => 'javascript: editProducts(this)',
                        'data-id-product' =>$model->id
                    ]);
            }
        ]
    ],
    'id',
    [
        'header' => 'Категория товара',
        'attribute' => 'fid_category',
        'value' => 'fidCategory.title',
        'filter' => \app\models\Categorys::getCategorys()
    ],
    [
        'header' => 'Подкатегория товара',
        'attribute' => 'fid_section',
        'value' => 'fidSection.title',
        'filter' => \app\models\Sections::getSections()
    ],
    'title',
    //'descr:ntext',
     'articul',
     'price',
    // 'link',
    // 'date_add',
    // 'img_path_small',
    // 'img_path_full',
    // 'fid_age',


];

?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить новый товар', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>
</div>

<!-- Modal EditProduct -->
<div class="modal fade" id="modal-edit-product" tabindex="-1" role="dialog" aria-labelledby="modal-edit-product">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Отмена"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal-edit-productLabel">Редактирование товара</h4>
        </div>
            <div class="modal-body">
                <div class="update-product">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

