<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Orders', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'export' => false,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'fid_products:ntext',
            'price',
            [
                'header' => 'Дата заказа',
                'attribute' => 'date_order',
                'value' => 'date_order',
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' =>[
                    'language' => 'ru',
                    'convertFormat' => true,
                    'pluginOptions'=>[
                        'timePicker24Hour' => true,
                        'timePicker' => true,
                        'timePickerIncrement' => 30,
                        'locale' => [
                            'format' => 'Y-m-d H:i:s'
                        ],
                    ]
                ]
            ],
            'email:email',
            'phone',
             'name',
            // 'notice:ntext',
             //'date_order',

            // 'date_send',
            // 'date_pay',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
