$(window.document).on('mouseover', '.dropdown-submenu', function() {$(this).addClass('open');});
$(window.document).on('mouseout', '.dropdown-submenu', function() {$(this).removeClass('open');});

//не переходим по ссылке
$('.order').find('.small-button').on('click', function(e) {
    e.preventDefault();

});

function addToOrder(e)
{
    var url;
    url = 'add-to-order?id='+$(e).data('id');

    $.ajax({
        url: url,
        type: 'GET',
        async: false,
        showLoader: true,
        success: function (data) {
            console.log(data);
            try{
                    $('.badge').find('a').html('Ваш заказ ('+data['count']+')');
                    $('.message_site').show().find('h2').html(data['text']);


                setTimeout(function () {
                    $('.message_site').hide('slow');
                }, 2000);
                return false;
            }
            catch(e)
            {
                console.log(e);
                return;
            }
        }
    });
}

$('document').ready(function(){
    //считаем итого по всем товарам
    var itogo = 0;
    for(var i = 0; i < $('.price').length; i++){
        itogo = itogo + Number($('.price').eq(i).html());
    }
    //выводим посчитанный результат
    $('.itogo').find('span').html(itogo);
    //кладем значени кол-во товара в скрытое поле, чтобы потом его использовать для отправки отчета на почту
    $('.order_block').find('.count').find('.c_prod').attr('value', 1);


});

//при добавлении или удалении товаров из заказа считается общая сумма
function counter(e)
{
    var num = $(e).parent().find('.count_prod').html();
    num = Number(num);
    if($(e).attr('name') == 'left'){
        if(num == 1) return;
        num--;
    }else if($(e).attr('name') == 'right'){
        if(num == 10) return;
        num++;
    }
    $(e).parent().find('.count_prod').html(num);
    $(e).parent().find('.c_prod').attr('value',num);
    var price = Number($(e).parent().find("input").val());
    price = price * num;
    $(e).parent().find('.price').html(price);

    var itogo = 0;
    for(var i = 0; i < $('.price').length; i++){
        itogo = itogo + Number($('.price').eq(i).html());
    }
    $('.itogo').find('span').html(itogo);
}