var defObj = {
    repeat: 5,
    type: 'get',
    url: 'search-products',
    data:	{

    },
    success: function(data){
        if(data){
            alert(data);
        }
    },
    error: function(e, str, t){
        console.log(e,str,t);
    }
};

/**
 * Вывод подсказок для поиска по продуктам
 *
 * @param input - инпут, в который вводится название продукта
 */
var connection = false;
function searchProducts(input)
{
    $(input).attr('data-val', '');
    if ($(input).val().length>=2) {
        //var form = $("#current-form").serializeArray();

        if(connection !== false) {
            connection.abort();
        }
        var tObj = Object.create(defObj);
        //tObj.data = $.param(form),
        tObj.data.q = $(input).val(),
            tObj.success = function(data){
                if(data){
                    var result = new Array;
                    for (var key in data) {
                        result.push({label: data[key].title  , value: data[key].title});
                    }
                    $(input).autocomplete({
                        source: result,
                        select: function(event, ui){
                            $(input).val(ui.item.label);
                            //$(input).attr('data-val', ui.item.value);
                            event.preventDefault();
                        }
                    });
                }
            },
            tObj.error = function(e, str, t){
                console.log(e,str,t);
            }
    }
    connection = $.ajax(tObj);
}