<?php
/**
 * Created by PhpStorm.
 * User: Konstantin
 * Date: 14.10.2016
 * Time: 21:03
 */

namespace app\components;
use yii\base\Widget;
use yii\helpers\Html;
use app\models\Products;
use yii\helpers\Url;


class ProductsOthers extends Widget
{
    public $count = 5;

    public $id;

    public $border = 'green';

    public function run()
    {
        $products = Products::find()->limit($this->count)->where(['not', ['id' => $this->id]])->orderBy("rand()")->all();
        $trs = '';
        foreach ($products as $product) {

            $img = Html::tag('img', null, ['src' => Url::to($product->pathImgSmall), 'alt' => $product->title]);
            $td_1 = Html::tag('td', $img);
            $text = Html::tag('a', $product->title, ['href' => $product->link]);
            $text .= Html::tag('span', $product->articul, ['class' => 'articul']);
            $td_2 = Html::tag('td', $text);
            $trs .= Html::tag('tr', $td_1.$td_2);
        }
        return Html::tag('table', $trs, ['id' => 'products_others', 'class' => 'products_others_'.$this->border]);
    }
}

?>

