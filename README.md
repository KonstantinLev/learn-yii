Yii2
============================

Документация:

- [yii2 на русском(полное руководство)](https://nix-tips.ru/yii2-api-guides/guide-ru-README.html)
- [Оффиц доки](http://www.yiiframework.com/doc-2.0/)
- [Вроде тоже есть что полезное](http://docs.mirocow.com/doku.php?id=yii2:start)





Структура проекта
-------------------

      assets/             Сюда добавляем наши css/js и зависимости из виджетов
      commands/           Можно создавать контроллеры с экшенами и вызывать их из консоли
      config/             Конфигурация проекта. Настраивается деволтные роуты, ЧПУ-ссылки, подключение к бд и тп
      controllers/        Контроллеры с экшенами - основа навигации
      mail/               Содержатся вьюхи email-писем, которые отправляются через модуль mailer
      models/             Содержит модели
      runtime/            Содержится кэш, логи
      tests/              Для написания юнит-тестов, yii юзает codeception
      vendor/             Сюда композером тянутся зависимости виджетов, компонентов и тп
      views/              Вьюшки
      web/                Точка входа на сайт. Ничего интересного, просто в index.php указываем, какой конфиг юзаем и все



Требования
------------

Минимальная версия пыхи -  PHP 5.4.0.


Установка
------------

### Через композер

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following command:

~~~
php composer.phar global require "fxp/composer-asset-plugin:~1.1.1"
php composer.phar create-project --prefer-dist --stability=dev yiisoft/yii2-app-basic basic
~~~

Now you should be able to access the application through the following URL, assuming `basic` is the directory
directly under the Web root.

~~~
http://localhost/basic/web/
~~~


Конфигурация
-------------

### База данных

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

**Заметки:**
- Такие дела

**Использование CRUD**
-----
Для использования данного генератора у нас должна быть модель. Далее:
- заходим в gii -> crud generate
- Указываем полный путь до модели начиная с "app", используем данный слеш '\\'
- Путь до Search-модели точно такой же, но в конце добавляем Search
- Указываем название контроллера (исходя из наших действий). Также полный путь от app
п.с. Например, если мы будет редактировать справочник городов, то EditCitiesController и тп
- указываем путь, где будут находиться вьюхи. Важно, путь начинается с алиаса "@web/". Используется отличный слеш. 
Конечная папка со вьюхами должна содержать название контроллера с маленькой буквы. Если
контроллер с составным название (EditCitiesController), то разделяем через дефис: edit-cities
---------------------
У нас сформируется контроллер, Search-модель и вьюхи. Вьюхи следующие:

```php
- _form.php
- _search.php
- create.php
- index.php
- update.php
- view.php
```
- _form - содержит набор полей (инпутов, чтобы вводить данные)
- _search - вызывается в index.php, по умолчанию закомментирован, используется, исли
надо отобразить фильтры не в таблице, а вне неё
- create - вьюзха рендерится, когда добавляем новую запись в модель(бд)
- Ну и далее впринципе из навзания ясно, что за что отвечает
