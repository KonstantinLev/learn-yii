<?php

namespace app\controllers\site;

use app\models\WriteUsList;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class BaseController extends Controller
{

    private $writeUsModel;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->getWriteUsModel();
        if(parent::beforeAction($action)){
            $this->module->setViewPath('@app/views/site');
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function getMenu()
    {

    }

    public function getProducts()
    {

    }

    public function getWriteUsModel(){

        if(is_null($this->writeUsModel)) {
            $this->writeUsModel = new WriteUsList();
        }
        Yii::$app->view->params['writeUsModel'] = $this->writeUsModel;
        return $this->writeUsModel;
    }


}
