<?php

namespace app\controllers\site;

use app\models\Products;
use app\models\WriteUsList;
use Yii;
use yii\filters\AccessControl;

use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\Response;
use yii\bootstrap\ActiveForm;
use yii\web\UploadedFile;

class MainController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'error', 'logout'],
                        'roles' => ['client'],
                    ],
                    [
                        'allow' => false,
                        'actions' => ['contacts'],
                        'roles' => ['client'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $writeUsModel = $this->getWriteUsModel();


        if (Yii::$app->request->isPost && $writeUsModel->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax ){
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($writeUsModel);
            } else {
                if (!$writeUsModel->save()) {
                    foreach ($writeUsModel->getErrors() as $error)
                        Yii::$app->session->addFlash('danger', print_r($error,1));
                }
            }

        }

        return $this->render('index', [
            'mainProducts' => Products::getMainProducts()
        ]);

    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        //parent::getWriteUsModel();

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

           // $model->file = UploadedFi\le::getInstance($model, 'file');
            //$model->file->saveAs('photo/'.$model->file->baseName.'.'.$model->file->extension);
            return $this->redirect('/main/index');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        //parent::getWriteUsModel();
        Yii::$app->user->logout();

        return $this->goHome();
        //return $this->redirect('/main/index');
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionContacts()
    {
        return $this->render('contacts');
    }
}
