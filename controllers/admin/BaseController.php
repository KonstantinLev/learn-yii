<?php

namespace app\controllers\admin;

use app\models\WriteUsList;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class BaseController extends Controller
{

    private $writeUsModel;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Что будет происходить перед вызовом экшена. В данному случае мы показываем контроллеру, где лежат вьюхи
     * @param $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if(parent::beforeAction($action)){
            $this->module->setViewPath('@app/views/admin');
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }


}
