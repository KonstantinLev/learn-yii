<?php

use yii\web\Request;

$params = require(__DIR__ . '/params.php');

$baseUrl = str_replace('/web', '', (new Request)->getBaseUrl());

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'defaultRoute' => 'main/index',
    'language' => 'ru_RU',
    'layout' => '@app/views/site/layouts/main',
    'controllerNamespace' => 'app\controllers\site',
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'jeNz2NjCsb-yD8GFo8acfrZ4SPjEevY8',
            //'baseUrl' => $baseUrl
            //'baseUrl' => '/'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'main/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'qwe30574@gmail.com',
                'password' => 'Pwnfclak',
                'port' => '465',
                'encryption' => 'ssl'
            ],
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            //свои правила, используем класс
            'rules' => [
                ['class' => 'app\components\SefRule', 'connectionID' => 'db']
            ]
        ],
        /*'assetManager' => [
            'basePath' => '@vendor/../web/assets',
            'baseUrl' => '@web/web/assets',
            'hashCallback' => function ($path) {
                return hash('crc32', $path);
            },
            'afterCopy' => function () {
                Yii::$app->params['loadingAssets'] = true;
            }
        ],*/

    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
